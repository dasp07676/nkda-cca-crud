const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const fileUpload = require('express-fileupload');

const app = express();
var corsOptions = {
  // origin: "http://localhost:8081"
  origin: "*"
};

const db = require("./helper/dbhelper");
db.connect();
app.use(cors(corsOptions));
app.use(fileUpload());

// parse requests of content-type - application/json
app.use(bodyParser.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));


//api routes
var citizen = require('./routes/citizen');
var caller = require('./routes/caller');
var sanitization = require('./routes/sanitization');
var patient = require('./routes/patient');
var deisolation_request = require('./routes/deisolation_request');
var email = require('./routes/email');
var caseVal = require('./routes/case');
var sanitization_team = require('./routes/sanitization_team');


app.use('/citizen', citizen);
app.use('/caller', caller);
app.use('/sanitization', sanitization);
app.use('/patient', patient);
app.use('/deisolation_request', deisolation_request);
app.use('/email', email);
app.use('/case', caseVal);
app.use('/sanitization_team', sanitization_team);

app.get("/", (req, res) => {
  res.json({ message: "Welcome to Api Application." });
});


// set port, listen for requests
const PORT = process.env.PORT || 8080;
app.listen(PORT, '0.0.0.0', () => {
  console.log(`Server is running on port ${PORT}.`);
});
