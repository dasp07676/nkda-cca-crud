const express = require('express');
const db = require("../helper/dbhelper");
const router = express.Router();
const moment = require("moment");

var timeNow = moment().format();
console.log(timeNow);

router.post("/submit/:id", (req, res) => {
    const id = req.params.id;
    const file = req.files;
    const lat = Number(req.query.lat).toFixed(7);
    const long = Number(req.query.long).toFixed(7);
    const accuracy = Number(req.query.accuracy).toFixed(7);
    
    console.log(lat);

    // If all the files are empty...
    if(file == null){
        res.status(400).send({"Message" : "Please capture atleast one photo of the affected area and then submit the form!!!"});
    }else{
        const file1 = req.files.image1;
        const file2 = req.files.image2;
        const file3 = req.files.image3;
        const file4 = req.files.image4;

        db.query(`select * from sanitization_request where id = ${id} AND status = 'Approved'`, (err1, result1) => {
            if(result1.rowCount == 1){
                // If file1 is not selected
                if(file1 == undefined){
                    console.log("File1 is not selected")
                }else{
                    file1.mv(`./resources/sanitizationCompleteImages" + ${file1.name}_${id}_${lat}_${long}_${timeNow}_1.jpg`, (err2, result2) => {
                        if(err2){
                            res.send(err2);
                        }else{
                            const path = `./resources/sanitizationCompleteImages" + ${file1.name}_${id}_${lat}_${long}_${timeNow}_1.jpg`;
                            db.query(`insert into images (sanitization_requests_id, path, "createdAt", "updatedAt") values (${id}, '${path}', NOW(), NOW())`, (err3, result3) => {
                                if(err3){
                                    res.status(400).send(err3);
                                }    
                            })
                        }
                    })
                }

                // If file2 is not selected
                if(file2 == undefined){
                    console.log("File2 is not selected");
                }else{
                    var timeNow2 = moment().format();
                    file2.mv(`./resources/sanitizationCompleteImages" + ${file2.name}_${id}_${lat}_${long}_${timeNow}_2.jpg`, (err4, result4) => {
                        if(err4){
                            res.send(err4);
                        }else{
                            const path = `./resources/sanitizationCompleteImages" + ${file2.name}_${id}_${lat}_${long}_${timeNow}_2.jpg`;
                            db.query(`insert into images (sanitization_requests_id, path, "createdAt", "updatedAt") values (${id}, '${path}', NOW(), NOW())`, (err5, result5) => {
                                if(err5){
                                    res.status(400).send(err5);
                                }    
                            })
                        }
                    })
                }

                // If file3 is not selected
                if(file3 == undefined){
                    console.log("File3 is not selected");
                }else{
                    file3.mv(`./resources/sanitizationCompleteImages" + ${file3.name}_${id}_${lat}_${long}_${timeNow}_3.jpg`, (err6, result6) => {
                        if(err6){
                            res.status(400).send(err6);
                        }else{
                            const path = `./resources/sanitizationCompleteImages" + ${file3.name}_${id}_${lat}_${long}_${timeNow}_3.jpg`;
                            db.query(`insert into images (sanitization_requests_id, path, "createdAt", "updatedAt") values (${id}, '${path}', NOW(), NOW())`, (err7, result7) => {
                                if(err7){
                                    res.status(400).send(err7);
                                }    
                            })
                        }
                    })
                }

                // If file4 is not selected
                if(file4 == undefined){
                    console.log("File4 is not selected");
                }else{
                    file4.mv(`./resources/sanitizationCompleteImages" + ${file4.name}_${id}_${lat}_${long}_${timeNow}_4.jpg`, (err8, result8) => {
                        if(err8){
                            res.status(400).send(err8);
                        }else{
                            const path = `./resources/sanitizationCompleteImages" + ${file4.name} + "_${id}_${lat}_${long}_${timeNow}_4.jpg"`;
                            db.query(`insert into images (sanitization_requests_id, path, "createdAt", "updatedAt") values (${id}, '${path}', NOW(), NOW())`, (err9, result9) => {
                                if(err9){
                                    res.status(400).send(err9);
                                }    
                            })
                        }
                    })
                }

                // Update the status of sanitization request Approved to Completed automatically
                let query = `update sanitization_request set status = 'Completed', "updatedAt"=NOW(), latitude=${lat}, longitude=${long}, accuracy=${accuracy} where id = ${id};`; 
                console.log(query);
                db.query(query, (err10, result10) => {
                    if(result10.rowCount == 1){
                        res.status(200).send({
                            "Status" : true,
                            "Message" : "File Uploaded",
                            "Details" : "Data inserted and status updated to Completed",
                            "Latitude" : lat,
                            "Longitude" : long,
                            "Accuracy" : accuracy
                        })
                    }
                })
            }
            else{
                res.status(400).send({"Message" : "Either this sanitization_request_id is not available in the database or this request is already been Completed"});
            }
        })
    }
})

module.exports = router;
