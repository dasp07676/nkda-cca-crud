const express = require('express');
const db = require("../helper/dbhelper");
const router = express.Router();

// Getting all the details of citizens
router.get('/getall', function (req, res) {
    db.query(`SELECT * FROM "case"`, (err, result) => {
        if (err) throw err;
        // console.log(result.rows);
        res.json(result.rows);
    })
});

// Get the details of a citizen by his/her id
router.get('/get/:id', function (req, res) {
    const citizen_id = req.params.id;
    db.query(`SELECT * FROM "case" WHERE citizen_id = ${citizen_id}`, (err, result) => {
        if (err) throw err;
        if(result.rows[0]){
            res.json(result.rows);
        }else{
            res.send({"Message" : "No citizen is available with this id in database..."});
        }
    })
});

module.exports = router;
