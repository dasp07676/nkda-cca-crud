const express = require('express');
const db = require("../helper/dbhelper");
const router = express.Router();

// Inserting citizen details...
router.post('/insert', (req, res) => {

    const mob_no = req.body.mob_no;
    const email = req.body.email;
    const name = req.body.name;
    const age = req.body.age;
    const sex = req.body.sex;
    const address = req.body.address;
    const action_area = req.body.action_area;
    const building = req.body.building;
    const createdAt = req.body.createdAt;
    const updatedAt = req.body.updatedAt;
    const is_active = req.body.is_active;
    const sample_collection_date = req.body.sample_collection_date;
    const place_of_admition = req.body.place_of_admition;
    const status_of_patient = req.body.status_of_patient;
    const isolation_status = req.body.isolation_status;
    const group_id = req.body.group_id;
    const direct_contuct = req.body.direct_contuct;
    const status = req.body.status;
    const no_of_direct_contact = req.body.no_of_direct_contact;
    const status_of_direct_contact = req.body.status_of_direct_contact;
    const result_of_direct_contact = req.body.result_of_direct_contact;
    const diabetes_report = req.body.diabetes_report;
    const flat_no = req.body.flat_no;
    const plot_no = req.body.plot_no;
    const street_no = req.body.plot_no;
    const landmark = req.body.landmark;
    const is_vaccinated = req.body.is_vaccinated;
    const hospital_name = req.body.hospital_name;
    const hospital_admition_date = req.body.hospital_admition_date;

    const mno = String(mob_no);
    const l = mno.length;

    if (l == 10) {
        db.query(`INSERT INTO "citizen"(id, mob_no, email, name, age, sex, address, action_area, building, "createdAt", "updatedAt", diabetes_report, flat_no, plot_no, street_no, landmark) values(nextval('citizen_sequence'), '${mob_no}','${email}','${name}', ${age}, '${sex}', '${address}', '${action_area}', '${building}', '${createdAt}', '${updatedAt}', '${diabetes_report}', '${flat_no}', '${plot_no}', '${street_no}', '${landmark}')`, (err, result) => {
            if (err) {
                console.log(err);
                res.status(400).send(err);
            }
            if (result.rowCount == 1) {
                // res.send({
                //     "Message" : "Data Inserted Successfully!!!"
                // })
                db.query(`SELECT * FROM citizen WHERE mob_no = '${mob_no}' AND name = '${name}';`, (err1, res1) => {
                    if (res1.rowCount != 0) {
                        // res.send(res1.rows[0]);
                        const citizen_id = res1.rows[0].id

                        // res.send({"Me" : citizen_id});
                        db.query(`INSERT INTO "case"(
                            id, citizen_id, is_active, sample_collection_date, place_of_admition, status_of_patient, isolation_status, group_id, "createdAt", "updatedAt", status, direct_contuct, no_of_direct_contact, result_of_direct_contact, status_of_direct_contact, is_vaccinated, hospital_name, hospital_admition_date)
                            VALUES (nextval('citizen_sequence'), ${citizen_id}, '${is_active}', '${sample_collection_date}', '${place_of_admition}', '${status_of_patient}', '${isolation_status}', ${group_id}, NOW(), NOW(), '${status}', '${direct_contuct}', ${no_of_direct_contact}, '${result_of_direct_contact}', '${status_of_direct_contact}', '${is_vaccinated}', '${hospital_name}', '${hospital_admition_date}');`, (err2, res2) => {
                            if (res2.rowCount == 1) {
                                res.send({ "Message": "Data inserted properly!!!" });
                            }
                            else {
                                res.send({ "Message": err2 })
                            }
                        })
                    }
                })
            }
        })
    } else {
        res.send({ "Message": "Mobile number has to be of 10 digits!!!" });
    }
})

//  Updating citizen details where citizen can only update their own data such as their mobile number, email, age, sex etc by their id
router.patch("/update/:id", (req, res) => {
    const id = req.params.id;
    const mob_no = req.body.mob_no;
    const email = req.body.email;
    const name = req.body.name;
    const age = req.body.age;
    const sex = req.body.sex;
    const address = req.body.address;
    const action_area = req.body.action_area;
    const building = req.body.building;
    const createdAt = req.body.createdAt;
    const updatedAt = req.body.updatedAt;
    const diabetes_report = req.body.diabetes_report;
    const flat_no = req.body.flat_no;
    const plot_no = req.body.plot_no;
    const street_no = req.body.plot_no;
    const landmark = req.body.landmark;
    const is_active = req.body.is_active;
    const sample_collection_date = req.body.sample_collection_date;
    const place_of_admition = req.body.place_of_admition;
    const status_of_patient = req.body.status_of_patient;
    const isolation_status = req.body.isolation_status;
    const group_id = req.body.group_id;
    const direct_contuct = req.body.direct_contuct;
    const status = req.body.status;
    const no_of_direct_contact = req.body.no_of_direct_contact;
    const status_of_direct_contact = req.body.status_of_direct_contact;
    const result_of_direct_contact = req.body.result_of_direct_contact;
    const is_vaccinated = req.body.is_vaccinated;
    const hospital_name = req.body.hospital_name;
    const hospital_admition_date = req.body.hospital_admition_date;



    const mno = String(mob_no);
    const l = mno.length;

    if (l == 10) {
        db.query(`UPDATE citizen SET mob_no = '${mob_no}', email = '${email}', name = '${name}', age = ${age}, sex = '${sex}', address = '${address}', action_area = '${action_area}', building = '${building}', "createdAt" = '${createdAt}', "updatedAt" = '${updatedAt}', diabetes_report = '${diabetes_report}', flat_no = '${flat_no}', plot_no = '${plot_no}', street_no = '${street_no}', landmark = '${landmark}' WHERE id = ${id};`, (err, result) => {
            if (err) {
                console.log(err);
            }
            if (result.rowCount != 0) {
                // res.status(200).send({ "Message": "Data updated properly!!!" });
                db.query(`SELECT * FROM citizen WHERE mob_no = '${mob_no}' AND name = '${name}';`, (err1, res1) => {
                    if (res1.rowCount != 0) {
                        // res.send(res1.rows[0]);
                        const citizen_id = res1.rows[0].id

                        // res.send({"Me" : citizen_id});
                        db.query(`UPDATE "case"
                        SET id=nextval('citizen_sequence'), citizen_id=${citizen_id}, is_active='${is_active}', sample_collection_date='${sample_collection_date}', place_of_admition='${place_of_admition}', status_of_patient='${status_of_patient}', isolation_status='${isolation_status}', group_id=${group_id}, "createdAt"=NOW(), "updatedAt"=NOW(), status='${status}', direct_contuct='${direct_contuct}', no_of_direct_contact=${no_of_direct_contact}, result_of_direct_contact='${result_of_direct_contact}', status_of_direct_contact='${status_of_direct_contact}', is_vaccinated='${is_vaccinated}', hospital_name='${hospital_name}', hospital_admition_date='${hospital_admition_date}'
                        WHERE citizen_id=${citizen_id};`, (err2, res2) => {
                            if(res2.rowCount == 1){
                                res.status(200).send({"Message" : "Data updated properly!!!"});
                            }else{
                                res.status(400).send(err2);
                            }
                        })
                    }
                })
            } 
            else {
                res.status(400).send({ "Message": "No citizen is available with this id!!!" });
            }
        })
    } else {
        res.status(400).send({ "Message": "Mobile number has to be of 10 digits!!!" });
    }
})


// Getting all the details of citizens
router.get('/getall', function (req, res) {

    db.query("SELECT * FROM citizen", (err, result) => {
        if (err) throw err;
        console.log(result.rows);
        res.json(result.rows);
    })
});

// Get all the details of a citizen along with their case by his/her id
router.get('/get/:id', function (req, res) {
    id = req.params.id;
    db.query(`SELECT * FROM citizen WHERE id = ${id}`, (err, result) => {
        if (err) throw err;
        if (result.rowCount != 0) {
            // var data1 = result.rows[0];
            db.query(`SELECT * FROM "case" WHERE citizen_id = ${id};`, (err2, result2) => {
                if (result2.rowCount != 0) {
                    res.status(200).send({ ...result.rows[0], ...result2.rows[0] });
                }
            })

        } else {
            res.send({ "Message": "No citizen is available with this id in database..." });
        }
    })
});

router.get('/get', function (req, res) {
    const name = req.body.name;
    db.query(`SELECT * FROM citizen WHERE name = '${name}'`, (err, result) => {
        if (err) throw err;
        if (result.rows[0]) {
            res.json(result.rows);
        } else {
            res.send({ "Message": "No citizen is available with this id in database..." });
        }
    })
});

// Delete a citizen by
// router.delete('/delete/:id', (req, res) => {
//     id = req.params.id
//     db.query(`DELETE from citizen WHERE id = ${id}`, (err, result) => {
//         if(err) throw err;
//         if(result.rowCount == 1){
//             res.status(200).send({"Message" : `Citizen, ${id} is deleted from database!!!`});
//         }else{
//             res.status(400).send({"Message" : "Citizen with this id is not available in database!!!"});
//         }
//     })
// })


module.exports = router;
