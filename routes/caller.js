const express = require('express');
const db = require("../helper/dbhelper");
const router = express.Router();

// Getting the details of a caller by his/her id
router.get('/get/:id', function (req, res) {
    id = req.params.id;
    db.query(`SELECT * FROM caller WHERE id = ${id}`, (err, result) => {
        if (err) throw err;
        if(result.rows[0]){
            res.json(result.rows);
        }else{
            res.send({"Message" : "No caller is available with this id in database..."});
        }
    })
});

// Reading caller details
router.post('/read', (req, res) => {
    const username = req.body.username;
    const password = req.body.password;
    // res.send(username + password);
    db.query(`SELECT * FROM caller WHERE "userName" = '${username}' and password = '${password}'`, (err, result) => {
        if(err) throw err;
        if(result.rowCount == 1){
            res.status(200).send(result.rows[0]);
        }else{
            res.status(400).send({"Message" : "Username or password is wrong!!!"});
        }
    })
})

// Update the details of a caller by his/her id
router.patch("/update/:id", (req, res) => {
    const id = req.params.id;
    const mob_no = req.body.mob_no;
    const name = req.body.name;
    const address = req.body.address;
    const username = req.body.username;
    const password = req.body.password;
    const createdAt = req.body.createdAt;
    const updatedAt = req.body.updatedAt;    

    const mno = String(mob_no);
    const l = mno.length;

    if(l == 10){
        db.query(`UPDATE caller SET (mob_no, name, address, "userName", "createdAt", "updatedAt") = ('${mob_no}','${name}','${address}', '${username}', '${createdAt}', '${updatedAt}') WHERE id = ${id}`, (err, result) => {
            if(err){
                console.log(err);
            }
            if(result.rowCount != 0){
                res.status(200).send({"Message" : "Caller details updated properly!!!"});
            }else{
                res.status(400).send({"Message" : "No caller is available with this id!!!"});
            }
        })
    }else{
        res.status(400).send({"Message" : "Mobile number has to be of 10 digits!!!"});
    }
})

// Update the password of a caller by his/her id
router.patch("/update_username_password", (req, res) => {
    const oldUsername = String(req.body.oldUsername);
    const newUsername = String(req.body.newUsername);
    const oldPassword = String(req.body.oldPassword);
    const newPassword = String(req.body.newPassword);

    db.query(`SELECT * FROM caller WHERE "userName" = '${oldUsername}' AND password = '${oldPassword}'`, (err1, result1) => {
        if(result1.rowCount == 1){
            const id = result1.rows[0].id
            db.query(`UPDATE caller SET "userName" = '${newUsername}', password = '${newPassword}' WHERE id = ${id}`, (err2, result2) => {
                if(result2.rowCount == 1){
                    res.status(200).send({"Message" : "New username and password is updated properly!!!"});
                }else{
                    res.status(400).send(err2);
                }
            })
        }
        else{
            res.status(400).send({"Message" : "Username or password is wrong!!!Please try again..."});
        }
    })
})

// Inserting details of caller
router.post('/insert', (req, res) => {
    const mob_no = req.body.mob_no;
    const name = req.body.name;
    const address = req.body.address;
    const username = req.body.username;
    const password = req.body.password;
    const createdAt = req.body.createdAt;
    const updatedAt = req.body.updatedAt;

    const mno = String(mob_no);
    const l = mno.length;

    if(l == 10){
        db.query(`INSERT INTO "caller" (mob_no, name, address, "userName", password, "createdAt", "updatedAt") values('${mob_no}','${name}', '${address}', '${username}', '${password}', '${createdAt}', '${updatedAt}')`, (err, result) => {
            if (err) {
                console.log(err);
                res.status(400).send(err);
            }
            if (res) {
                res.send({
                    "Message" : "Data Inserted Successfully!!!"
                })
            }
        })
    }else{
        res.send({"Message" : "Mobile number has to be of 10 digits!!!"});
    }
})

// Caller updates the details of diabetic patients after confirmation by the patients/citizens if anyone is there in the database...
router.patch("/update_citizen/:id", (req, res) => {
    const id = req.params.id;
    const mob_no = req.body.mob_no;
    const email = req.body.email;
    const name = req.body.name;
    const age = req.body.age;
    const sex = req.body.sex;
    const address = req.body.address;
    const action_area = req.body.action_area;
    const building = req.body.building;
    const diabetes_report = req.body.diabetes_report;

    // res.send(diabetes_report);

    const mno = String(mob_no);
    const l = mno.length;

    if(l == 10){
        db.query(`UPDATE citizen SET mob_no = ${mob_no}, email = '${email}', name = '${name}', age = ${age}, sex = '${sex}', address = '${address}', action_area = '${action_area}', building = '${building}',  "createdAt" = NOW(), "updatedAt" = NOW(), diabetes_report = '${diabetes_report}' WHERE id = ${id}`, (err, result) => {
            if(err){
                console.log(err);
            }
            if(result.rowCount != 0){
                res.status(200).send({"Message" : "Data updated properly!!!"});
            }else{
                res.status(400).send({"Message" : "No citizen is available with this id!!!"});
            }
        })
    }else{
        res.status(400).send({"Message" : "Mobile number has to be of 10 digits!!!"});
    }
})

module.exports = router;
