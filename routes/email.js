const express = require('express');
const db = require("../helper/dbhelper");
const router = express.Router();

router.post('/insert/:id', (req, res) => {
    const email = req.body.email;
    const citizen_id = req.params.id;

    db.query(`SELECT * FROM citizen WHERE id = ${citizen_id}`, (err1, result1) => {
        if (err1) throw err;

        if(result1.rowCount == 1){
            // res.json(result1.rows);
            db.query(`UPDATE citizen SET email = '${email}' WHERE id = ${citizen_id}`, (err, result) => {
                if(result.rowCount == 1){
                    res.status(200).send({"Message" : "Email is updated properly!!!"});      
                }else{
                    res.status(200).send(err);
                }
            })
        }else{
            res.send({"Message" : "No citizen is available with this id in database..."});
        }
    })
})

module.exports = router;