const express = require('express');
const db = require("../helper/dbhelper");
const router = express.Router();

router.post("/insert", (req, res) => {
    const citizen_id = req.body.citizen_id;

    db.query(`SELECT * FROM citizen WHERE id = ${citizen_id}`, (error4, result4) => {
        if (result4.rowCount != 0) {

            db.query(`SELECT * FROM "case" WHERE citizen_id = ${citizen_id}`, (err1, result1) => {
                const case_id = result1.rows[0].id;

                // res.send({"Case-id" : case_id});
                // console.log(case_id);
                db.query(`SELECT * FROM sanitization_request WHERE case_id = ${case_id} ORDER BY "createdAt" DESC LIMIT 1`, (err2, result2) => {
                    if (result2.rowCount != 1) {
                        // res.send("Ok");
                        db.query(`INSERT INTO sanitization_request (case_id, "createdAt", "updatedAt") VALUES (${case_id}, NOW(), NOW())`, (err3, result3) => {
                            if (result3.rowCount == 1) {
                                res.send({
                                    "Message": `Citizen ${citizen_id} has requested for sanitization!!!`,
                                    ...result4.rows[0],
                                    ...result1.rows[0]
                                });
                            }
                        })
                    }
                    else if (result2.rows[0].status == "Requested") {
                        res.send({ "Message": "A citizen can only request again only if the previous request is completed or rejected" });
                    }
                    else if (result2.rows[0].status == "Completed" || result2.rows[0].status == "Rejected") {
                        db.query(`INSERT INTO sanitization_request (case_id, "createdAt", "updatedAt") VALUES (${case_id}, NOW(), NOW())`, (err3, result3) => {
                            if (result3.rowCount == 1) {
                                res.send({ 
                                    "Message": `Citizen ${citizen_id} has requested again for sanitization!!!`,
                                    ...result4.rows[0],
                                    ...result1.rows[0]
                                });
                            }
                        })
                    }
                    else {
                        res.send({ "Message": "Request is under process" });
                    }
                })
            })
        }
        else{
            res.status(400).send({"Message" : `Citizen ${citizen_id} is not available in the database!!!`});
        }
    })
})

router.patch("/update/:id", (req, res) => {
    const status = req.body.status;
    const id = req.params.id;
    // const id = req.body.id;

    db.query(`UPDATE sanitization_request SET status = '${status}' WHERE id = ${id}`, (err, result) => {
        if (err) throw err;

        if (result.rowCount == 1) {
            res.status(200).send({ "Message": "Sanitization status updated properly!!!" });
        } else {
            res.status(400).send({ "Message": "Sanitization request not found!!!" });
        }
    })
})

// Directly Rejecting the sanitization request
router.patch("/reject/:id", (req, res) => {
    const status = "Rejected";
    const id = req.params.id;

    db.query(`UPDATE sanitization_request SET status = '${status}' WHERE id = ${id}`, (err, result) => {
        if (err) throw err;

        if (result.rowCount == 1) {
            res.status(200).send({ "Message": "Sanitization request is Rejected!!!" });
        } else {
            res.status(400).send({ "Message": "Sanitization request not found!!!" });
        }
    })
})

// Get all the sanitization requests requested by the citizens
router.get("/getall", (req, res) => {
    db.query("SELECT * FROM sanitization_request", (err, result) => {
        if (err) throw err;
        console.log(result.rows);
        res.json(result.rows);
    })
})

// Get all sanitization requests by status
router.get("/getstatus", (req, res) => {
    db.query("SELECT * FROM sanitization_request ORDER BY id DESC", (err, result) => {
        if (err) throw err;
        res.json(result.rows);
    })
})

// Get sanitization requests by a particular status
router.get("/getstatus/:status", (req, res) => {
    const status = req.params.status;
    db.query(`select s.id,s.approved_by,s.assign_to,s.status,s.action_by,(s."createdAt"),(s."updatedAt"),ca.citizen_id,ca.is_active,ca.sample_collection_date,ca.place_of_admition,
    ca.status_of_patient,ca.isolation_status,ca.group_id,ca.status,ca.direct_contuct,ca.no_of_direct_contact,ca.result_of_direct_contact, 
    ca.status_of_direct_contact,ca.is_vaccinated,ca.hospital_name,ca.hospital_admition_date,ci.mob_no,ci.email,ci.name,ci.age,ci.sex,ci.address,ci.action_area,
    ci.building,ci.diabetes_report,ci.flat_no,ci.plot_no,ci.street_no,ci.landmark from
    sanitization_request s inner join "case" ca
    ON s.case_id = ca.id
    inner join citizen ci
    ON ca.citizen_id = ci.id
    where s.status = '${status}';`, (err, result) => {
        if(err) throw err;
        if(result.rows != 0){
            res.status(200).send(result.rows);
        }else{
            res.status(400).send({"Message" : "This type of request is not available in the database"});
        }
    })    
})

// If a citizen has requested more than once then
router.get("/get/:id", (req, res) => {
    const citizen_id = req.params.id;
    // res.send(citizen_id);
    db.query(`SELECT * FROM "case" WHERE citizen_id = ${citizen_id}`, (err1, result1) => {
        if (result1.rowCount != 1) {
            res.send({ "Message": "Citizen with this id is not available in the database!!!" });
        }
        else {
            const case_id = result1.rows[0].id;
            // console.log(case_id);
            db.query(`SELECT * FROM sanitization_request WHERE case_id = ${case_id}`, (err, result) => {
                if (err) throw err;
                // console.log(result.rows);
                if (result.rowCount >= 1) {
                    res.json(result.rows);
                } else {
                    res.send({ "Message": "Citizen has not requested for any sanitization!!!" });
                }
            })
        }
    })
})

module.exports = router;
